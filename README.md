# Move.it

This project is a contribution to the Pomodoro method, gamifying it and bringing a new way to keep people exercising during their workday.

It was conceived during Rocketseat's fourth Next Level Week (NLW4) which took place between February 22-28, 2021

Layout implemented by me, but created by Rocketseat avaliable on:
- [Move.it 2.0](https://www.figma.com/file/HzLflCBwhLqr4leFBLbCtj/Move.it-2.0)

### Built With

[Visual Code 1.54.1](https://code.visualstudio.com/Download) - IDE

### Main dependencies

- [mongoose](https://www.npmjs.com/package/mongoose): MongoDB object modeling tool.
- [next](https://nextjs.org): A React Framework.

### directory layout structure

    .
    ├── .next          # next build folder
    ├── .vercel        # vercel build folder
    ├── node_modules   # node dependencies folders
    ├── src            # main folder containing all app components and codes
    │   ├── components # reusable components or micro pages fragments
    │   ├── contexts   # contexts to share informations between pages
    │   ├── database   # database interface
    │   ├── pages      # pages / routes avaliables
    │   └── styles     # CSS elements
    └── ...            # others files to configure this repository and project

## Deploy

You can deploy it on your own Vercel account [![Deploy with Vercel](https://vercel.com/button)](https://vercel.com/new/git/external?repository-url=https://gitlab.com/sebas.sil/moveit&project-name=moveit&repository-name=moveit). Or click on [moveit](https://moveit-sebas-sil.vercel.app/) to see it running.

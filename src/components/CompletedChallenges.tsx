import {useChallenge} from '../contexts/ChallengeContext'
import style from '../styles/CompletedChallenges.module.css'

const CompletedChallenges = () => {

  const { completedChallenges } = useChallenge()
  
  return (
    <div className={style.container}>
      <span>Desafios completados</span>
      <span>{completedChallenges}</span>
    </div>
  )
}

export default CompletedChallenges

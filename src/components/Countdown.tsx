import {useCountdown} from '../contexts/CountdownContext'
import style from '../styles/Countdown.module.css'

const Countdown = () => {

  const { minutes, seconds, hasFinished, isActive, start, stop } = useCountdown()

  const [ml, mr] = String(minutes).padStart(2, '0').split('')
  const [sl, sr] = String(seconds).padStart(2, '0').split('')

  return (
    <>
      <div className={style.container}>
        <div><span>{ml}</span><span>{mr}</span></div>
        <span>:</span>
        <div><span>{sl}</span><span>{sr}</span></div>
      </div>
      <button disabled={hasFinished} type='button' className={[style.button, isActive ? style.buttonActive : ''].join(' ')} onClick={isActive ? stop: start}>
        { hasFinished ? 'Finalizou' : (isActive ? 'Abandonar ' : 'Iniciar ') } o ciclo
      </button>
    </>
  )
}

export default Countdown

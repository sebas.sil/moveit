import {useAuth} from '../contexts/AuthContext'
import {useChallenge} from '../contexts/ChallengeContext'
import style from '../styles/Profile.module.css'

const Profile = () => {

  const { level } = useChallenge()
  const { name, avatar } = useAuth()
  
  return (
    <div className={style.container}>
      <img src={avatar} alt="avatar image"/>
      <div>
        <strong>{name}</strong>
        <p>
          <img src="icons/level.svg" alt="icon"/>
          level {level}</p>
      </div>
    </div>
  )
}

export default Profile

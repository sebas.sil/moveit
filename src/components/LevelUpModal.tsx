import style from '../styles/LevelUpModal.module.css'

interface LevelUpModalProps {
  level:number
  setShowModal: (val:boolean) => void
}
const LevelUpModal = ({level, setShowModal}: LevelUpModalProps) => {
  return (
    <div className={style.overlay}>
      <div className={style.container}>
        <header>{level}</header>
        <strong>Parabéns</strong>
        <p>Você alcançou um novo nível</p>
        <button onClick={() => setShowModal(false)}>
          <img src="/icons/close.svg" alt="close button"/>
        </button>
      </div>
    </div>
  )
}

export default LevelUpModal

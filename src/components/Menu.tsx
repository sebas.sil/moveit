import style from '../styles/Menu.module.css'
import Logo from '../../public/icons/logo.svg'
import Home from '../../public/icons/home.svg'
import Medal from '../../public/icons/medal.svg'
import Policy from '../../public/icons/policy.svg'
import {useEffect} from 'react'
import Link from 'next/link'

interface MenuProps {
  actived: 'home' | 'ranking' | 'policy'
}
const Menu = ({actived}:MenuProps) => {

  useEffect(() => {
    document.querySelector('.' + style.actived)?.classList.remove(style.actived)
    document.querySelector('.' + style[actived]).classList.add(style.actived)
  }, [actived])

  return (
    <div className={style.container}>
      <Logo />
      <div className={style.menu}>
        <div className={style.home}><Link href='home'><Home/></Link></div>
        <div className={style.ranking}><Link href='ranking'><Medal /></Link></div>
        <div className={style.policy}><Link href='policy'><Policy /></Link></div>
      </div>
    </div>
  )
}

export default Menu

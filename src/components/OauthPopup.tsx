import {ReactNode, useEffect} from 'react'

interface Props {
  width: number
  height: number
  url: string
  title: string
  onClose: () => any
  onCode: (code: string) => void
  children?: ReactNode
}

const OauthPopup = (props: Props) => {

  let externalWindow: any;

  useEffect(() => {

    // componentWillUnmount
    return (externalWindow) && externalWindow.close()
  }, [])

  function createPopup () {
    const {url, title, width, height, onCode} = props;
    const left = window.screenX + (window.outerWidth - width) / 2;
    const top = window.screenY + (window.outerHeight - height) / 2.5;

    const windowFeatures = `toolbar=0,scrollbars=1,status=1,resizable=0,location=1,menuBar=0,width=${width},height=${height},top=${top},left=${left}`;

    externalWindow = window.open(
      url,
      title,
      windowFeatures
    );

    const storageListener = () => {
      try {
        if (localStorage.getItem('code')) {
          onCode(localStorage.getItem('code'));
          externalWindow.close();
          window.removeEventListener('storage', storageListener);
        }
      } catch (e) {
        window.removeEventListener('storage', storageListener);
      }
    }

    window.addEventListener('storage', storageListener);

    externalWindow.addEventListener('beforeunload', () => {
      props.onClose()
    }, false);
  };

  return (
    <div onClick={createPopup}>
      {props.children}
    </div>
  )
}

export default OauthPopup

import style from '../styles/ErrorMessage.module.css'

interface IErrorMessageProps {
  message: string
}
const ErrorMessage = ({message}: IErrorMessageProps) => {
  return (
    <div className={style.error}>{message}</div>
  )
}

export default ErrorMessage

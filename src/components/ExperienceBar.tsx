import { useChallenge } from '../contexts/ChallengeContext'
import styles from '../styles/ExperienceBar.module.css'

const ExperienceBar = () => {

  const { currentExperience, nextLevelExperience } = useChallenge()

  const toNext = Math.round((currentExperience/nextLevelExperience)*100)

  return (
    <header className={styles.experienceBar}>
      <span>0 xp</span>
      <div>
        <div style={{width: `${toNext}%`}} />
        <span style={{left: `${toNext}%` }}>{currentExperience} xp</span>
      </div>
      <span>{nextLevelExperience} xp</span>
    </header>
  )
}

export default ExperienceBar

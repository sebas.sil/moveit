import {useChallenge} from '../contexts/ChallengeContext'
import {useCountdown} from '../contexts/CountdownContext'
import style from '../styles/ChallengeBox.module.css'

const ChallengeBox = () => {

  const { challenge, resetChallenge, completeChallenge } = useChallenge()
  const { stop } = useCountdown()
  
  function handleSucceed(){
    resetChallenge()
    completeChallenge()
    stop()
  }

  function handleFail(){
    resetChallenge()
    stop()
  }

  return (
    <div className={style.container}>
      {challenge ? (
        <div className={style.active}>
          <header>{challenge.amount}</header>
          <main>
            <img src={`icons/${challenge.type}.svg`} alt="body icon"/>
            <strong>Novo desafio</strong>
            <p>{challenge.description}</p>
          </main>

          <footer>
            <button type='button' className={style.failedButton} onClick={handleFail}>Falhei</button>
            <button type='button' className={style.succeededButton} onClick={handleSucceed}>Completei</button>
          </footer>
        </div>
      ) : (
      <div className={style.notActive}>
        <strong>
          Finalize um ciclo para receber um desafio.
        </strong>
        <p>
          <img src="icons/level-up.svg" alt="level up icon"/>
          Avance de nivel completando desafios.
        </p>
      </div>
      )}
    </div>
  )
}

export default ChallengeBox

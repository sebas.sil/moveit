import {useEffect, useState} from 'react'
import ErrorMessage from '../components/ErrorMessage'
import Menu from '../components/Menu'
import {useAuth} from '../contexts/AuthContext'
import style from '../styles/Policy.module.css'
import Router from 'next/router'

interface IPolicy {
  version_number: number,
  version_label: string,
  create: Date,
  body: [{tag: string, value: string}]
}

const Policy = () => {

  const [policy, setPolicy] = useState<IPolicy>()
  const [message, setMessage] = useState<string>()

  const {login, avatar, name, policy: accepted, policyAccepted} = useAuth()

  useEffect(() => {

    fetch('/api/policyService').then(e => {
      if (e.status === 200) {
        return e.json()
      } else {
        return e.json().then(e => setMessage(JSON.stringify(e.error)))
      }
    }).then(setPolicy)
  }, [])

  function sendPolicy() {
    fetch('/api/policyService', {
      method: 'POST',
      body: JSON.stringify({login, avatar, name}),
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
      }
    }).then(async (e) => {
      if (e.status === 200) {
        return e.json()
      } else {
        return e.json().then(e => {console.log(e.error); throw new Error(e.error)})
      }
    }).then(e => {policyAccepted(); Router.push('home')}).catch(e => setMessage(e.message))
  }

  return (
    <>
      {message && <ErrorMessage message={message} />}
      <Menu actived='policy' />

      <div className={style.container}>
        {policy && policy.body.map((e, i) => (
          e.tag === 'h1' ? <h1 key={i}>{e.value}</h1> :
            e.tag === 'h2' ? <h2>{e.value}</h2> :
              e.tag === 'p' && <p>{e.value}</p>
        ))}
        {!accepted && <div className={style.footer_container}>
          <div>
            Você aceita estes termos?
            <div className={style.btn_container}>
              <button onClick={sendPolicy} className={style.btn_yes}>SIM</button>
              <button className={style.btn_no}>Não</button>
            </div>
          </div>
        </div>}


      </div>
    </>
  )
}

export default Policy

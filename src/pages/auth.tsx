import { GetServerSideProps } from 'next'
import {useEffect} from 'react'

interface AuthProps {
  code: string
}

const Auth = ({code} : AuthProps) => {
  useEffect(() => {
    localStorage.setItem('code', code)
  }, [])
  return <div />
}

export const getServerSideProps:GetServerSideProps = async ({query}) => {
  // tudo que esta aqui roda no back-end
  const { code = '' } = query
  return {
    props: {
      code
    }
  }
}

export default Auth

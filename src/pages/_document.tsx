import Document, { Html, Head, Main, NextScript } from 'next/document'

export default class MyDocument extends Document {
  render() {
    return (
      <Html>
        <Head>
          <meta property="og:description" content="Gamifica a aplicação do processo Pomodoro, que divide tarefas em intervalos de tempo para maximizar a produtividade, tornando seu uso mais atrativo para os usuários e insentivando exercícios durante o dia de trabalho." />
          <meta property="og:url" content="https://moveit-sebas-sil.vercel.app" />
          <meta property="og:title" content="move.it" />
          <meta property="og:image" content="/site_image.png" />

          <meta content="summary" property="twitter:card"/>
          <meta content="move.it" property="twitter:title"/>
          <meta property="twitter:description" content="Gamifica a aplicação do processo Pomodoro, que divide tarefas em intervalos de tempo para maximizar a produtividade, tornando seu uso mais atrativo para os usuários e insentivando exercícios durante o dia de trabalho." />
          <meta property="twitter:image" content="/site_image.png" />
          
          <meta content="move.it" property="title"/>
          <meta property="description" content="Gamifica a aplicação do processo Pomodoro, que divide tarefas em intervalos de tempo para maximizar a produtividade, tornando seu uso mais atrativo para os usuários e insentivando exercícios durante o dia de trabalho." />
          <meta property="image" content="/site_image.png" />

          <link rel="shortcut icon" href="favicon.png" type="image/png"/>
          <link rel="preconnect" href="https://fonts.gstatic.com" />
          <link href="https://fonts.googleapis.com/css2?family=Inter:wght@400;500;600&family=Rajdhani:wght@600&display=swap" rel="stylesheet" />
        </Head>

        <body>
          <Main />
          <NextScript />
        </body>
      </Html>
    )
  }
}

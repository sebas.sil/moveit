import Menu from '../components/Menu'
import style from '../styles/Ranking.module.css'
import LevelUp from '../../public/icons/level.svg'
import {useEffect, useState} from 'react'
import ErrorMessage from '../components/ErrorMessage'

const Ranking = () => {

  const [users, setUsers] = useState([])
  const [message, setMessage] = useState<string>()

  useEffect(() => {
    fetch('/api/rankingService').then(e => {
      if(e.status === 200) {
        return e.json()
      } else {
        return e.json().then(e => {throw new Error(e.error)})
      }
    }).then(setUsers).catch(e => setMessage(e.message))
  }, [])

  return (
    <>
      <Menu actived='ranking' />
      <div className={style.container}>
        {message && <ErrorMessage message={message} />}
        <h1>Leaderboard</h1>
        <div className={style.board}>
          <table>
            <thead>
              <tr>
                <th>posição</th>
                <th>usuário</th>
                <th>desafios</th>
                <th>experiência</th>
              </tr>
            </thead>
            <tbody>
              {
                users.map((e, i) => (
                  <tr key={i}>
                    <td>{i+1}</td>
                    <td>
                      <div className={style.user}>
                        <img src={e.avatar} alt="avatar" />
                        <div><p>{e.name}</p><p><LevelUp />Level {e.level}</p></div>
                      </div>
                    </td>
                    <td><span>{e.challenges}</span> finalizados</td>
                    <td><span>{e.experience}</span> xp</td>
                  </tr>
                ))
              }
            </tbody>
            <tfoot></tfoot>
          </table>
        </div>
      </div>
    </>
  )
}

export default Ranking

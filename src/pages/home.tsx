import { GetServerSideProps } from 'next'
import ChallengeBox from '../components/ChallengeBox'
import CompletedChallenges from '../components/CompletedChallenges'
import Countdown from '../components/Countdown'
import ExperienceBar from '../components/ExperienceBar'
import Profile from '../components/Profile'
import {CountdownProvider} from '../contexts/CountdownContext'

import style from '../styles/Home.module.css'
import {ChallengeProvider} from '../contexts/ChallengeContext'
import Menu from '../components/Menu'
import {useEffect, useState} from 'react'
import {useAuth} from '../contexts/AuthContext'

export default function Home() {

  const { login } = useAuth()

  const [level, setLevel] = useState<number>()
  const [experience, setExperience] = useState<number>()
  const [challenges, setChallenges] = useState<number>()
  const [isLoading, setIsLoading] = useState<boolean>(true)
  

  useEffect(() => {
    if(login){
      fetch('/api/rankingService?login=' + login).then(e => e.json()).then(userData =>{
        setLevel(userData.level)
        setExperience(userData.experience)
        setChallenges(userData.challenges)
        setIsLoading(false)
      })
    } else {
      setLevel(1)
      setExperience(0)
      setChallenges(0)
      setIsLoading(false)
    }
  }, [])

  
  return (isLoading ? <div /> :
    <ChallengeProvider level={level} currentExperience={experience} completedChallenges={challenges}>
      <Menu actived='home'/>
      <div className={style.container}>
        <ExperienceBar />
        <CountdownProvider>
          <section>
            <div>
              <Profile />
              <CompletedChallenges />
              <Countdown />
            </div>
            <div>
              <ChallengeBox />
            </div>
          </section>
        </CountdownProvider>
      </div>
    </ChallengeProvider>
  )
}

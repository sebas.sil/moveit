import {NowRequest, NowResponse} from '@vercel/node'
import { getOne } from '../../database/controller'

interface IAuthenticatedUser {
  name: string
  avatar: string
  login: string
}
async function getAccessToken(code: string): Promise<string> {
  return fetch('https://github.com/login/oauth/access_token', {
    method: 'POST',
    body: JSON.stringify({client_id: process.env.NEXT_PUBLIC_CLIENT_ID, client_secret: process.env.CLIENT_SECRET, code}),
    headers: {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    }
  }).then(response => response.json()).then(e => e.access_token)
}

async function getUserData(access_token: string): Promise<IAuthenticatedUser> {
  return fetch('https://api.github.com/user', {
    headers: {
      'Accept': 'application/json',
      'Authorization': 'token ' + access_token
    }
  }).then(response => response.json()).then(data => {
    return {
      name: data.name,
      avatar: data.avatar_url,
      login: data.login
    }
  })
}

const Authenticator = async (request: NowRequest, response: NowResponse) => {

  try {
    const {code} = request.query

    if (code) {
      const user = await getAccessToken(code as string).then(getUserData)
      let userData = await getOne(user.login)
      return response.json(userData || user)
    } else {
      throw new Error('code is required')
    }
  } catch (error) {
    console.error(error)
    response.status(401).json({error: error.message})
  }

}

export default Authenticator

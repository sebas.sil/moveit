import {NowRequest, NowResponse} from '@vercel/node'
import {getLastPolicy, insertOne } from '../../database/controller'


const PolicyService = async (request: NowRequest, response: NowResponse) => {
  if(request.method === 'GET'){
    return getLastPolicy().then(response.json).catch(e => response.status(400).json({error:e}))
  } else if(request.method === 'POST'){
    const { login, avatar, name } = request.body
    if(login && avatar && name){
      return insertOne(login, avatar, name, 1, 0, 0).then(response.json).catch(e => response.status(400).json({error:e}))
    } else {
      return new Promise(() => response.status(403).json({error:'É necessária a autenticação para aceitar as politicas.'}))
    }
  } else {
    return response.status(404).json({})
  }
}

export default PolicyService

import {NowRequest, NowResponse} from '@vercel/node'
import {updateOne, getAll, getOne, IUser, insertOne} from '../../database/controller'


const update = async (login:string, level:number, challenges:number, experience:number): Promise<IUser> => {
  return updateOne(login, level, challenges, experience)
}

const RankingService = async (request: NowRequest, response: NowResponse) => {
  if(request.method === 'GET'){
    const { login } = request.query
    if(login){
      return getOne(login as string).then(response.json).catch(e => response.status(400).json({error:e}))
    } else {
      return getAll().then(response.json).catch(e => response.status(400).json({error:e}))
    }
  } else if (request.method === 'POST'){
    const { login, level, challenges, experience } = request.body
    if(login){
      return update(login, level, challenges, experience).then(response.json).catch(response.send)
    } else {
      return response.status(403).json({})
    }
  } else if (request.method === 'DELETE'){
    const { login } = request.query
    console.log('delete', login)
  } else {
    return response.status(404).json({})
  }
}

export default RankingService

import style from '../styles/Login.module.css'
import Logo from '../../public/logo-full.svg'
import GitHub from '../../public/icons/github.svg'
import Guest from '../../public/icons/person.svg'
import OauthPopup from '../components/OauthPopup'
import Home from '../pages/home'
import {useAuth} from '../contexts/AuthContext'
import {MouseEvent, useState} from 'react'
import ErrorMessage from '../components/ErrorMessage'
import Router from 'next/router'

interface UserData {
  name: string
  avatar: string
  login: string
  level: number
  challenges: number
  experience: number
  ula: {accepted: Date}
}

const Login = () => {

  const [isAuthenticated, setIsAuthenticated] = useState(false)
  const [message, setMessage] = useState<string>()

  const {setName, setAvatar, setLogin, policyAccepted} = useAuth()

  async function onCode(code: string) {
    try {
      const user: UserData = await fetch('/api/authenticator?code=' + code)
      .then(e => {
        if(e.status === 200) {
          return e.json()
        } else {
          return e.json().then(e => {throw new Error(e.error)})
        }
      })

      if (Object.keys(user).length !== 0) {
        setName(user.name)
        setAvatar(user.avatar)
        setLogin(user.login)
        if(user.ula?.accepted){
          policyAccepted()
        }
        setIsAuthenticated(true)

        if(!user.ula){
          Router.push('/policy')
        }
      }
    } catch (e) {
      setMessage(e.message)
    } finally {
      window.localStorage.removeItem('code'); //remove code from localStorage
    }
  }

  function guestAuthentication(event: MouseEvent<HTMLAnchorElement>){
    event.preventDefault()
    const guest = {
      name: 'Convidado',
      avatar: '/icons/person.svg',
      login: undefined,
      level: 1,
      challenges: 0,
      experience: 0
    }
    setName(guest.name)
    setAvatar(guest.avatar)
    setIsAuthenticated(true)
  }

  return (
    isAuthenticated ? <Home /> :
      <div className={style.container}>
        <section>
          <div className={style.left}><img src="/symbol.svg" alt="symbol image" /></div>
          <div className={style.right}>
            <Logo />
            <p>Bem Vindo</p>
            <div>
            {message && <ErrorMessage message={message} />}
              <OauthPopup
                url={`https://github.com/login/oauth/authorize?client_id=${process.env.NEXT_PUBLIC_CLIENT_ID}`}
                width={400}
                height={430}
                onCode={onCode}
                onClose={() => {}}
                title="GitHub">
                <a className={style.github} href='#'>
                  <GitHub />
                  <p>Login with GitHub</p>
                </a>
              </OauthPopup>
              <a className={style.guest} href='#' onClick={guestAuthentication}>
                <Guest />
                <p>Entrar sem login</p>
              </a>
            </div>
          </div>
        </section>
      </div>
  )
}

export default Login

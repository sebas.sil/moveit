import {createContext, ReactNode, useContext, useState} from 'react'

interface AuthProviderData {
  login: string
  name: string
  avatar: string
  policy: boolean
  setLogin: (login: string) => void
  setName: (name: string) => void
  setAvatar: (avatar: string) => void
  policyAccepted: () => void
}

interface AuthProviderProps {
  children: ReactNode
}

const AuthContext = createContext({} as AuthProviderData)

const AuthProvider = ({children}: AuthProviderProps) => {

  const [login, setLogin] = useState<string>()
  const [name, setName] = useState<string>()
  const [avatar, setAvatar] = useState<string>()
  const [policy, setPolicy] = useState<boolean>()

  function policyAccepted(){
    setPolicy(true)
  }
  return (
    <AuthContext.Provider value={{login, name, avatar, policy, setLogin, setName, setAvatar, policyAccepted}}>
      {children}
    </AuthContext.Provider>
  )

}

const useAuth = () => {
  return useContext(AuthContext)
}


export {AuthProvider, useAuth}

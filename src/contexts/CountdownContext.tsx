import { createContext, ReactNode, useContext, useEffect, useState } from 'react'
import { useChallenge } from '../contexts/ChallengeContext'

interface CountdownContextData {
  minutes: number
  seconds: number
  hasFinished: boolean
  isActive: boolean
  start: () => void
  stop: () => void
}

interface CountdownProviderProps {
  children: ReactNode
}

const CountdownContext = createContext({} as CountdownContextData)

const CountdownProvider = ({children}: CountdownProviderProps) => {

  const { startNewChallenge } = useChallenge()

  const initialTime = 25 * 60
  const [time, setTime] = useState(initialTime)
  const [isActive, setIsActive] = useState(false)
  const [hasFinished, setHasFinished] = useState(false)

  const minutes = Math.floor(time / 60)
  const seconds = time % 60

  let timer: NodeJS.Timeout

  useEffect(() => {
    if(isActive){
      if(time > 0) {
        timer = setTimeout(() => {
          setTime(time -1)
        }, 1000);
      } else {
        setHasFinished(true)
        setIsActive(false)
        startNewChallenge()
      }
    }
  }, [isActive, time])

  function start() {
    setIsActive(true);
  }

  function stop() {
    clearTimeout(timer)
    setIsActive(false)
    setTime(initialTime)
    setHasFinished(false)
  }

  return (
    <CountdownContext.Provider value={{minutes, seconds, hasFinished, isActive, start, stop}}>
      {children}
    </CountdownContext.Provider>
  )
}

const useCountdown = () => {
  return useContext(CountdownContext)
}


export { CountdownProvider, useCountdown }

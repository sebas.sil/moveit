import {createContext, ReactNode, useContext, useEffect, useState} from 'react'
import challenges from '../../challenges.json'
import LevelUpModal from '../components/LevelUpModal'
import {useAuth} from './AuthContext'

interface ChallengeObject {
  type: 'body' | 'eye',
  description: string
  amount: number
}
interface ChallengeProviderData {
  level: number
  currentExperience: number
  completedChallenges: number
  challenge: ChallengeObject
  nextLevelExperience: number
  startNewChallenge: () => void
  resetChallenge: () => void
  completeChallenge: () => void
}

interface ChallengeProviderProps {
  children: ReactNode
  level: number
  currentExperience: number
  completedChallenges: number
}

const ChallengeContext = createContext({} as ChallengeProviderData)

const ChallengeProvider = ({children, ...rest}: ChallengeProviderProps) => {

  const { login } = useAuth()
  const [level, setLevel] = useState<number>(rest.level)
  const [currentExperience, setCurrentExperience] = useState<number>(rest.currentExperience)
  const [completedChallenges, setCompletedChallenges] = useState<number>(rest.completedChallenges)
  const [challenge, setChallenge] = useState(null as ChallengeObject)
  const [showModal, setShowModal] = useState(false)

  const dificulty = 4
  const nextLevelExperience = Math.pow((level + 1) * dificulty, 2)

  useEffect(() => {
    Notification.requestPermission()
  }, [])

  useEffect(() => {
    const user = {
      login, 
      level, 
      experience: currentExperience, 
      challenges: completedChallenges
    }
    console.log('user', user)
    fetch('/api/rankingService', {
      method: 'POST', 
      body: JSON.stringify(user), 
      headers: {
        'Accept': 'application/json',
        'Content-Type': 'application/json'
    }})
  }, [level, currentExperience, completedChallenges])

  function levelUp() {
    setLevel(level + 1)
    setShowModal(true)
  }

  function startNewChallenge() {
    const index = Math.floor(Math.random() * challenges.length)
    const newChallenge = challenges[index] as ChallengeObject
    setChallenge(newChallenge)

    new Audio('/notification.mp3').play()
    if (Notification.permission == 'granted') {
      new Notification('Novo desafio', {
        body: `Um novo desafio valendo ${newChallenge.amount}xp`,
        silent: true
      })
    }
  }

  function resetChallenge() {
    setChallenge(null)
  }

  function completeChallenge() {
    if (challenge) {
      const {amount} = challenge
      let exp = currentExperience + amount

      // BUGFIX se existir um desafio que faca o usuario subir mais de um nivel ao mesmo tempo, esse calculo esta errado
      if (exp >= nextLevelExperience) {
        exp -= nextLevelExperience
        levelUp()
      }

      setCurrentExperience(exp)
      setChallenge(null)
      setCompletedChallenges(completedChallenges + 1)
    }
  }

  return (
    <ChallengeContext.Provider value={{level, currentExperience, completedChallenges, challenge, nextLevelExperience, startNewChallenge, resetChallenge, completeChallenge}}>
      {children}
      {showModal && <LevelUpModal level={level} setShowModal={setShowModal} />}
    </ChallengeContext.Provider>
  )
}

const useChallenge = () => {
  return useContext(ChallengeContext)
}


export {ChallengeProvider, useChallenge}

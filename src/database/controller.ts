import mongoose from 'mongoose';

const Schema = mongoose.Schema
const UserModelSchema = new Schema({
  login: {type: String, required: true, unique: true},
  avatar: {type: String, required: true},
  name: {type: String, required: true},
  level: {type: Number, required: true},
  challenges: {type: Number, required: true},
  experience: {type: Number, required: true},
  ula: {
    _id: {
      type: mongoose.Schema.Types.ObjectId, 
      ref: 'ulas'
    },
    accepted: {type: Date, required: true}
  },
});
const PolicyModelSchema = new Schema({
  version_number: Number,
  version_label: String,
  create: Date,
  body: Array
});

let UserModel:mongoose.Model<any>
let PolicyModel:mongoose.Model<any>

const db = mongoose.connection

console.log('db.readyState', db.readyState)
if(db.readyState === 0){
  db.on('error', console.error.bind(console, 'MongoDB connection error:'))
  UserModel = mongoose.model('users', UserModelSchema)
  PolicyModel = mongoose.model('ulas', PolicyModelSchema)

  mongoose.connect('mongodb+srv://moveit:hRCUV6fZ5ue6Kr9@cluster0.9q0zd.mongodb.net/moveit', {
    useNewUrlParser: true,
    useUnifiedTopology: true,
    useFindAndModify: false,
    useCreateIndex: true
  })
} else {
  UserModel = mongoose.model('users')
  PolicyModel = mongoose.model('ulas')
}


interface IUser {
  login: string
  avatar: string
  name: string
  level: number
  challenges: number
  experience: number
}

interface IPolicy {
  version_number: number,
  version_label: string,
  create: Date,
  body: [{}]
}

async function insertOne(login: string, avatar: string, name: string, level: number, challenges: number, experience: number): Promise<IUser> {
  console.log('insert one', login)
  return PolicyModel.findOne().sort({_id:-1}).limit(1).exec().then(ula => {
    const user = new UserModel({login, avatar, name, level, challenges, experience, ula: {_id: ula._id, accepted: new Date().toISOString()}})
    console.log('inside', user)
    return user.save()
  })
}

async function updateOne(login: string, level: number, challenges: number, experience: number): Promise<IUser> {
  console.log('update one', login)
  return UserModel.findOneAndUpdate({login:login}, {level, challenges, experience}, {new: true})
}

async function getAll(): Promise<IUser[]> {
  console.error('get all')
  return UserModel.find().limit(10).exec()
}

async function getOne(login: string): Promise<IUser> {
  console.log('get one', login)
  return UserModel.findOne({login}).populate('users').exec()
}

async function getLastPolicy(): Promise<IPolicy> {
  console.log('get last policy')
  return PolicyModel.findOne().sort({_id:-1}).limit(1).exec()
}



export type {IUser}
export {insertOne, updateOne, getAll, getOne, getLastPolicy}

# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

- implementar o layout v2: https://www.figma.com/file/HzLflCBwhLqr4leFBLbCtj/Move.it-2.0 
- implementar share with Facebook
- implementar share with Twitter
- implementar artigo diário curto
- mover os exercicios para o banco
## [2.0.0]

- autenticacao com github
- entrar como convidado
- adicionado ranking
- adicionado politica de uso
- correcao de bug para abandonar ciclo

## [1.0.0]

- Cronometro regressivo
- Desafio
- Experiência e nível

[1.0.0]: https://gitlab.com/sebas.sil/moveit/-/releases/1.0.0
[2.0.0]: https://gitlab.com/sebas.sil/moveit/-/releases/2.0.0
